#ifndef _DOT_H_
#define _DOT_H_

#include "pvector.h"


class dot {
    private : 
        Pvector pos;
        Pvector vel;
        Pvector acc;
    public : 
        dot() : pos(), vel(0,0), acc(0,0){}
        
        //void show();
        
        void move();

};



#endif