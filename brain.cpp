#include "brain.h"
#include <cstdlib>
#define PI 3,1415926

brain::randomize(){
    float randomAngle;
    for(int i = 0; i < directions.size(); i++){
        randomAngle = rand(2*PI);
        direction[i] = Pvector(randomAngle);
    }
}