class dot{
  PVector pos;
  PVector vel;
  PVector acc;
  brain b;
  boolean dead = false;
  boolean finished = false;
  boolean reachedGoal = false;
  boolean isBest = false;
  float fitness = 0;
  
  dot(){
    b = new brain(500);
    pos = new PVector(800, 800);
    vel = new PVector(0,0); 
    acc = new PVector(0,0);
  }
  
  
  void show(){
    if(isBest){
      fill(0,255,0);
      ellipse(pos.x, pos.y, 8,8);
    }
    else{
      fill(0);
      ellipse(pos.x, pos.y, 4, 4);
    }
  }
  
  void move(){
    
    if(b.directions.length > b.step){
      acc = b.directions[b.step];
      b.step++;
    }
    else {
      //finished = true;
      dead = true;
    }
    vel.add(acc);
    vel.limit(5);
    pos.add(vel);
  }

  void update(){
    if(!dead && !reachedGoal){
      move();
      if(pos.x < 2 || pos.y < 2 || pos.x > width-2 || pos.y > height-2){
        dead = true;
      }
      else if (dist(pos.x, pos.y, goal1.x, goal1.y) < 25) { //|| dist(pos.x, pos.y, goal2.x, goal2.y) < 25)
        reachedGoal = true;
      }
      else if(hitSomethingBad()){
        dead = true;
      }
    }
  }
  
  void calculateFitness() {
    //if(dead){
      //return;
    //}
    if (reachedGoal) {
      fitness = 1.0/16.0 + 10000.0/(float)(b.step * b.step);
    } else {
      float distanceToGoal = dist(pos.x, pos.y, goal1.x, goal1.y); //+ dist(pos.x, pos.y, goal2.x, goal2.y);
      fitness = 1.0/(distanceToGoal * distanceToGoal);
    }
  }

  dot creatingBaby(){
    dot baby = new dot();
    baby.b = b.clone();
    return baby;
  }

  boolean hitSomethingBad(){
    if(pos.x > 400 && pos.y > 500 && pos.x < 1000 && pos.y < 510 ){
      return true;
    }
    //if(pos.x > 500 && pos.y > 50 && pos.x < 510 && pos.y < 450){
      //return true;
    //}
    //if(pos.x > 500 && pos.y > 560 && pos.x < 510 && pos.y < 960){
      //return true;
    //}
    if(pos.x > 300 && pos.y > 300 && pos.x < 310 && pos.y < 700){
      return true;
    }
    if(pos.x > 100 && pos.y > 300 && pos.x < 800 && pos.y < 310){
      return true;
    }
    if(pos.x > 500 && pos.y > 375 && pos.x < 510 && pos.y < 525){
      return true;
    }
    if(pos.x > 700 && pos.y > 275 && pos.x < 710 && pos.y < 425){
      return true;
    }
        
    if(pos.x > 100 && pos.y > 600 && pos.x < 300 && pos.y < 610){
      return true;
    }
    if(pos.x > 0 && pos.y > 450 && pos.x < 200 && pos.y < 460){
      return true;
    }
    return false;
  }
  
}
