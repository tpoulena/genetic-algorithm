population p;
PVector goal1 = new PVector(200,100);
//PVector goal2 = new PVector(width+800,100);

void setup(){
  size(1000,1000);
  p = new population(1000);  
}

void draw(){
  background(255);
  fill(255,0,3);
  ellipse(goal1.x, goal1.y, 50,50);
  //ellipse(goal2.x, goal2.y, 50,50);
  
  int obs1_x = 400;
  int obs1_y = 500;
  //int obs2_x = 500;
  //int obs2_y = 50;
  //int obs3_x = 500;
  //int obs3_y = 560;
  
  rect(obs1_x,obs1_y,900,10);
  rect(300, 300, 10, 400);
  rect(100, 300, 700, 10);
  rect(500, 375, 10, 150);
  rect(700, 275, 10, 150);
  
  rect(100,600, 200, 10);
  rect(0, 450, 200,10);
  //rect(obs2_x,obs2_y,10,400);
  //rect(obs3_x,obs3_y,10,400);
  
  if(p.allDotsDead()){
    p.calculateFitness();
    
    p.naturalSelection();
    
    p.mutate();
  }
  else {
    p.update();
    p.show();
  
  }
  

}
