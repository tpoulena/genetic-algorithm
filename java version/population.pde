class population{
  dot[] dots;
  float fitnessSum;
  int gen = 1;
  int bestDot = 0;
  int minStep = 500;
  
  population(int size){
    dots = new dot[size];
    for(int i = 0; i < size; i++){
      dots[i] = new dot();
    }
  }


  void show(){
    for(int i = 1; i < dots.length; i++){
      dots[i].show();
    }
    dots[0].show();
  }
  
  
  void update(){
    for(int i = 0; i < dots.length; i++){
      if(dots[i].b.step > minStep){
        dots[i].dead = true;
      }
      else {
        dots[i].update();
      }
      
    }
  }


  void calculateFitness(){
    for(int i = 0; i < dots.length; i++){
      dots[i].calculateFitness();
    }
  }

  boolean allDotsDead(){
    for(int i = 0; i < dots.length; i++){
      if(!dots[i].dead && !dots[i].reachedGoal && !dots[i].finished){
        return false;
      }  
    }
    return true;
  }

  void naturalSelection(){
    dot[] newDots = new dot[dots.length];
    setBestDot();
    calculateFitnessSum();
    
    newDots[0] = dots[bestDot].creatingBaby();
    newDots[0].isBest = true;
    
    for(int i = 1; i < newDots.length; i++){
      dot parent = selectParent();
      
      newDots[i] = parent.creatingBaby();
    }
    
    dots = newDots.clone();
    gen ++;
    
  }

  void calculateFitnessSum(){
    fitnessSum = 0;
    for(int i = 0; i< dots.length; i++){
      fitnessSum += dots[i].fitness;
    }
  }

  dot selectParent(){
    float rand = random(fitnessSum);
    float runningSum = 0;
    
    for(int i = 0; i< dots.length; i++){
      runningSum += dots[i].fitness;
      if(runningSum >= rand){
        return dots[i];
      }
    }
    return null;
  }



  void mutate(){
    for(int i = 1; i < dots.length; i++){
      dots[i].b.mutate();
    }
  }

  void setBestDot(){
    float max = 0;
    int maxIndex = 0;
    for(int i = 0; i < dots.length; i++){
      if(dots[i].fitness > max){
        max = dots[i].fitness;
        maxIndex = i;
      }
    }
    bestDot = maxIndex;
    
    if(dots[bestDot].reachedGoal){
      minStep = dots[bestDot].b.step;
    }
    
  }




}
